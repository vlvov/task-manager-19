package ru.t1.vlvov.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

}
