package ru.t1.vlvov.tm.service;

import ru.t1.vlvov.tm.api.service.ILoggerService;

import java.io.FileInputStream;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    private final static String CONFIG_FILE = "logger.properties";

    private final static String COMMAND = "COMMAND";

    private final static String COMMAND_FILE = "./command.log";

    private final static String ERROR = "ERROR";

    private final static String ERROR_FILE = "./error.log";

    private final static String MESSAGE = "MESSAGE";

    private final static String MESSAGE_FILE = "./message.log";

    private final static LogManager LOG_MANAGER = LogManager.getLogManager();

    private final static Logger LOGGER_ROOT = Logger.getLogger("");

    private final static Logger LOGGER_COMMAND = getLoggerCommand();

    private final static Logger LOGGER_ERROR = getLoggerError();

    private final static Logger LOGGER_MESSAGE = getLoggerMessage();

    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        init();
        registry(LOGGER_COMMAND, COMMAND_FILE, false);
        registry(LOGGER_ERROR, ERROR_FILE, true);
        registry(LOGGER_MESSAGE, MESSAGE_FILE, true);
    }

    public static Logger getLoggerCommand() {
        return Logger.getLogger(COMMAND);
    }

    public static Logger getLoggerError() {
        return Logger.getLogger(ERROR);
    }

    public static Logger getLoggerMessage() {
        return Logger.getLogger(MESSAGE);
    }

    private void init() {
        try {
            LOG_MANAGER.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final Exception e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + '\n';
            }
        });
        return handler;
    }

    private void registry(final Logger logger, final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (final Exception e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.fine(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e.getMessage() == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }

}
