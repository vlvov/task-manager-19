package ru.t1.vlvov.tm.exception.field;

public class EmailEmptyException extends AbstractFieldNotFoundException {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}