package ru.t1.vlvov.tm.exception.user;

public class IncorrectLoginOrPasswordException extends AbstractUserException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Incorrect login or password...");
    }

}